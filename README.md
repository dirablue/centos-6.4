yum -y groupinstall "Development Tools"

yum --enablerepo=epel -y install libyaml libyaml-devel readline-devel ncurses-devel gdbm-devel tcl-devel openssl-devel db4-devel libffi-devel

added epel repository
/etc/yum.repos.d/epel.repo

ruby 1.9.3p545
gem 1.8.23.2
knife-solo 0.4.1
nginx

